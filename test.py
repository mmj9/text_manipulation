# 2.
with open("example_text.txt", 'r', encoding='utf8') as f:
	data = f.readline()
	print("Readline yields:\n")
	print(data)

with open("example_text.txt", 'r', encoding='utf8') as f:
	data = f.readlines()
	print("Readlines yields:\n")
	print(data)
 
# 4.
# rstrip is only a method on a readline() object, NOT a readlines() object
# so in order to remove the '\n's from all the list elements in readlines()
# object, a for loop through all elements using rstrip(\n) must be used
 
for x in data:
    x.rstrip('\n')
    print(x)
    
# 5.
def count_men(filename):
    """
    Counts the occurrence of the substring "men" in a given file
    """
    with open(filename, 'r', encoding='utf8') as f:
        data = f.readlines()

    mencount = 0        
    for x in data:
        n = x.count("men")
        mencount += n
        
    return mencount
    
# 6.
def capitalize_women(filename):
    """
    Capitalizes all occurrences of the word women to become WOMEN in file
    """
    women = "women"    
    
    with open(filename, 'r', encoding='utf8') as f:
        data = f.readlines()
        
    for i, v in enumerate(data):
        if women in v:
            data[i] = v.replace("women", "WOMEN")
            
    return data
    
# 7.
def contains_blue_devil(filename):
    """
    Finds whether "Blue Devil" is contained within file
    Returns simple boolean for if it is present
    """
    bluedevil = "Blue Devil"
    
    with open(filename, 'r', encoding='utf8') as f:
        data = f.readlines()
        
    for x in data:
        if bluedevil in x:
            return True
    
    return False
    
